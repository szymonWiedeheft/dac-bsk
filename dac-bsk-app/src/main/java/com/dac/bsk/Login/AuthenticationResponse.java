package com.dac.bsk.Login;

/**
 * Created by SzymonW on 4/7/2017.
 */
public class AuthenticationResponse {
    private String token;
    private String name;
    private String surname;
    private String login;
    private long id;

    public AuthenticationResponse(String token, String name, String surname, String login, long id) {
        this.token = token;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getLogin() {
        return login;
    }

    public long getId() {
        return id;
    }
}
