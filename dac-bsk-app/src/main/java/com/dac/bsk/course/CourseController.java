package com.dac.bsk.course;

import com.dac.bsk.room.Room;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by SzymonW on 6/1/2017.
 */
@RestController
@RequestMapping(path = "/course")
public class CourseController {
    private CourseRepository courseRepository;

    public CourseController(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }


    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@accessController.hasPrivilege('course', 's') or @accessController.hasPrivilege('course', 'S')")
    public List<Course> findAll() {
        return courseRepository.findAll().stream()
                .sorted(Comparator.comparing(Course::getId))
                .collect(Collectors.toList());
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("@accessController.hasPrivilege('course', 's') or @accessController.hasPrivilege('course', 'S')")
    public Course get(@PathVariable long id) {
        return courseRepository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("@accessController.hasPrivilege('course', 'i') or @accessController.hasPrivilege('course', 'I') or @accessController.hasPrivilege('course', 'u') or @accessController.hasPrivilege('course', 'U')")
    public Course create(@RequestBody Course course) {
        return courseRepository.save(course);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("@accessController.hasPrivilege('course', 'u') or @accessController.hasPrivilege('course', 'U')")
    public Course update(@RequestBody Course course) {
        return courseRepository.save(course);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("@accessController.hasPrivilege('course', 'd') or @accessController.hasPrivilege('course', 'D')")
    public void delete(@PathVariable long id) {
        courseRepository.delete(id);
    }

}
