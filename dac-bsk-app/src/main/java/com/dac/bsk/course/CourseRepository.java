package com.dac.bsk.course;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by SzymonW on 4/8/2017.
 */
public interface CourseRepository extends JpaRepository<Course, Long> {
}
