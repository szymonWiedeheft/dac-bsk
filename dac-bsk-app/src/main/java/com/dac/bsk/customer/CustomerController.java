package com.dac.bsk.customer;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by SzymonW on 6/1/2017.
 */
@RestController
@RequestMapping(path = "/customer")
public class CustomerController {
    private CustomerRepository customerRepository;


    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@accessController.hasPrivilege('customer', 's') or @accessController.hasPrivilege('customer', 'S')")
    public List<Customer> findAll() {
        return customerRepository.findAll().stream()
                .sorted(Comparator.comparing(Customer::getId))
                .collect(Collectors.toList());
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("@accessController.hasPrivilege('customer', 's') or @accessController.hasPrivilege('customer', 'S')")
    public Customer get(@PathVariable long id) {
        return customerRepository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("@accessController.hasPrivilege('customer', 'i') or @accessController.hasPrivilege('customer', 'I') or accessController.hasPrivilege('customer', 'u') or @accessController.hasPrivilege('customer', 'U')")
    public Customer create(@RequestBody Customer customer) {
        return customerRepository.save(customer);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("@accessController.hasPrivilege('customer', 'u') or @accessController.hasPrivilege('customer', 'U')")
    public Customer update(@RequestBody Customer customer) {
        return customerRepository.save(customer);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("@accessController.hasPrivilege('customer', 'd') or @accessController.hasPrivilege('customer', 'D')")
    public void delete(@PathVariable long id) {
        customerRepository.delete(id);
    }
}
