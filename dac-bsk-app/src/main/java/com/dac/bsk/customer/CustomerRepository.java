package com.dac.bsk.customer;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by SzymonW on 4/8/2017.
 */

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
