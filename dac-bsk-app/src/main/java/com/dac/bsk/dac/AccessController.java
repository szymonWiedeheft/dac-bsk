package com.dac.bsk.dac;

import com.dac.bsk.dac.user.User;
import com.dac.bsk.dac.user.UserRepository;
import com.dac.bsk.dac.user.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

/**
 * Created by SzymonW on 5/31/2017.
 */
@Component
public class AccessController {

    private UserService userService;

    public AccessController(UserService userService) {
        this.userService = userService;
    }


    public boolean hasPrivilege(String resourceName, String privilege) {
        User user = userService.getUserByLogin(userService.getActiveUser().getLogin());
        for(UserPrivilegeOnResource userPrivilegeOnResource : user.getUserPrivilegeOnResourcesRgt()) {
            if(userPrivilegeOnResource.getPrivilegeOnResource().getPrivilege().getName().equals(privilege)
                    && userPrivilegeOnResource.getPrivilegeOnResource().getResource().getTableName().equals(resourceName)) {
                return true;
            }
        }
        return false;
    }

    public boolean isAdmin() {
        return userService.getActiveUser().getLogin().equals("admin");
    }


}
