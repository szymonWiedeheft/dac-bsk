package com.dac.bsk.dac;

import com.dac.bsk.dac.privilege.Privilege;

import javax.persistence.*;
import java.util.List;

/**
 * Created by SzymonW on 4/8/2017.
 */
@Entity
@Table(name = "privilege_on_resource")
public class PrivilegeOnResource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "privilege_id")
    private Privilege privilege;

    @ManyToOne
    @JoinColumn(name = "resource_id")
    private Resource resource;

    @OneToMany(mappedBy = "privilegeOnResource")
    private List<UserPrivilegeOnResource> userPrivilegeOnResources;

    @OneToMany(mappedBy = "privilegeOnResource")
    private List<UserPriviledgeHistory> userPriviledgeHistory;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Privilege getPrivilege() {
        return privilege;
    }

    public void setPrivilege(Privilege privilege) {
        this.privilege = privilege;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public List<UserPrivilegeOnResource> getUserPrivilegeOnResources() {
        return userPrivilegeOnResources;
    }

    public void setUserPrivilegeOnResources(List<UserPrivilegeOnResource> userPrivilegeOnResources) {
        this.userPrivilegeOnResources = userPrivilegeOnResources;
    }

    public List<UserPriviledgeHistory> getUserPriviledgeHistory() {
        return userPriviledgeHistory;
    }

    public void setUserPriviledgeHistory(List<UserPriviledgeHistory> userPriviledgeHistory) {
        this.userPriviledgeHistory = userPriviledgeHistory;
    }
}
