package com.dac.bsk.dac;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by SzymonW on 4/8/2017.
 */
@Entity
@Table(name = "resource")
public class Resource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "table_name")
    private String tableName;

    @OneToMany(mappedBy = "resource")
    private List<PrivilegeOnResource> privilegeOnResources;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<PrivilegeOnResource> getPrivilegeOnResources() {
        return privilegeOnResources;
    }

    public void setPrivilegeOnResources(List<PrivilegeOnResource> privilegeOnResources) {
        this.privilegeOnResources = privilegeOnResources;
    }
}
