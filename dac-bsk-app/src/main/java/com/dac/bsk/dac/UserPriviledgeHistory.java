package com.dac.bsk.dac;

import com.dac.bsk.dac.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by SzymonW on 4/8/2017.
 */
@Entity
@Table(name = "user_priviledge_history")
public class UserPriviledgeHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_from_id")
    private User userFrom;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_to_id")
    private User userTo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "priviledge_on_resource_id")
    private PrivilegeOnResource privilegeOnResource;

    @Column(name = "action")
    private String action;

    public UserPriviledgeHistory(Date date, User userFrom, User userTo, PrivilegeOnResource privilegeOnResource, String action) {
        this.date = date;
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.privilegeOnResource = privilegeOnResource;
        this.action = action;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(User userFrom) {
        this.userFrom = userFrom;
    }

    public User getUserTo() {
        return userTo;
    }

    public void setUserTo(User userTo) {
        this.userTo = userTo;
    }

    public PrivilegeOnResource getPrivilegeOnResource() {
        return privilegeOnResource;
    }

    public void setPrivilegeOnResource(PrivilegeOnResource privilegeOnResource) {
        this.privilegeOnResource = privilegeOnResource;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
