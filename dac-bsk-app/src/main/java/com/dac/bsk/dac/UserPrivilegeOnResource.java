package com.dac.bsk.dac;

import com.dac.bsk.dac.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by SzymonW on 4/8/2017.
 */
@Entity
@Table(name = "user_privilege_on_resource")
public class UserPrivilegeOnResource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "privilege_on_resource_id")
    private PrivilegeOnResource privilegeOnResource;

    @ManyToOne
    @JoinColumn(name = "lft")//kto
    private User lft;

    @ManyToOne
    @JoinColumn(name = "rgt")//komu
    private User rgt;

    public UserPrivilegeOnResource() {}

    public UserPrivilegeOnResource(PrivilegeOnResource privilegeOnResource, User lft, User rgt) {
        this.privilegeOnResource = privilegeOnResource;
        this.lft = lft;
        this.rgt = rgt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PrivilegeOnResource getPrivilegeOnResource() {
        return privilegeOnResource;
    }

    public void setPrivilegeOnResource(PrivilegeOnResource privilegeOnResource) {
        this.privilegeOnResource = privilegeOnResource;
    }

    public User getLft() {
        return lft;
    }

    public void setLft(User lft) {
        this.lft = lft;
    }

    public User getRgt() {
        return rgt;
    }

    public void setRgt(User rgt) {
        this.rgt = rgt;
    }
}
