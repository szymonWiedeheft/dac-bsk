package com.dac.bsk.dac.privilege;

import com.dac.bsk.dac.PrivilegeOnResource;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by SzymonW on 4/8/2017.
 */
@Entity
@Table(name = "privilege")
public class Privilege {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "privilege")
    private List<PrivilegeOnResource> privilegeOnResources;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PrivilegeOnResource> getPrivilegeOnResources() {
        return privilegeOnResources;
    }

    public void setPrivilegeOnResources(List<PrivilegeOnResource> privilegeOnResources) {
        this.privilegeOnResources = privilegeOnResources;
    }
}
