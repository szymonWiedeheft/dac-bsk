package com.dac.bsk.dac.privilege;

import com.dac.bsk.dac.PrivilegeOnResource;
import com.dac.bsk.dac.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

/**
 * Created by SzymonW on 6/2/2017.
 */
@RestController
@RequestMapping(path = "/privilege")
public class PrivilegeController {
    private UserService userService;
    private PrivilegeService privilegeService;


    public PrivilegeController(UserService userService, PrivilegeService privilegeService) {
        this.userService = userService;
        this.privilegeService = privilegeService;
    }

    @RequestMapping(path = "/transfer", method = RequestMethod.POST)
    @Transactional
    public ResponseEntity<?> transferPrivilege(@RequestBody PrivilegeTransferRequest privilegeTransferRequest) {
        if (!privilegeService.isTransferAcceptable(privilegeTransferRequest)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Nie mozna przekazac praw");
        }

        if (privilegeService.hasTakeOver(userService.getUserByLogin(privilegeTransferRequest.getRecipient()))) {
            privilegeService.removeTakeOver(userService.getUserByLogin(privilegeTransferRequest.getRecipient()), userService.getActiveUser());

            boolean isRecipientAdmin = userService.isAdmin(userService.getUserByLogin(privilegeTransferRequest.getRecipient()));
            if(!isRecipientAdmin) {
                takeAllPrivilegesFromRecipient(privilegeTransferRequest);
            }

            Map<String, List<String>> privileges = userService.getUserPrivileges(userService.getActiveUser().getId());
            for(Map.Entry<String, List<String>> pair : privileges.entrySet()) {
                for(String value : pair.getValue()) {
                    PrivilegeOnResource privilegeOnResource = privilegeService.getPrivilegeOnResource(value, pair.getKey());
                    if(privilegeOnResource.getPrivilege().getName().equals("przejmij")) {
                        continue;
                    }
                    if(!isRecipientAdmin) {
                        privilegeService.transferPrivilege(
                                privilegeOnResource,
                                userService.getUserByLogin(privilegeTransferRequest.getRecipient()),
                                userService.getUserByLogin(privilegeTransferRequest.getRecipient()));
                        privilegeService.createHistory(
                                privilegeOnResource,
                                userService.getUserByLogin(privilegeTransferRequest.getRecipient()),
                                userService.getActiveUser(),
                                "przekazano poprawnie");
                    }
                    privilegeService.takeAllPrivileges(
                            privilegeOnResource,
                            userService.getActiveUser(),
                            userService.getUserByLogin(privilegeTransferRequest.getRecipient())
                    );
                }
            }
            return ResponseEntity.ok("przekazano poprawnie");
        }

        privilegeTransferRequest.getPrivileges().forEach(privilege -> {
            PrivilegeOnResource privilegeOnResource = privilegeService.getPrivilegeOnResource(privilege, privilegeTransferRequest.getTable());
            if(privilegeService.isPrivilegeWithGrant(privilegeOnResource) && !privilegeService.isUpgradeRequest(userService.getUserByLogin(privilegeTransferRequest.getRecipient()), privilegeTransferRequest.getPrivileges(), privilegeTransferRequest.getTable())) {
                privilegeService.transferPrivilege(
                        privilegeService.getPrivilegeOnResource(privilege.toLowerCase(), privilegeTransferRequest.getTable()),
                        userService.getUserByLogin(privilegeTransferRequest.getRecipient()),
                        userService.getActiveUser());
            }
            privilegeService.transferPrivilege(
                    privilegeOnResource,
                    userService.getUserByLogin(privilegeTransferRequest.getRecipient()),
                    userService.getActiveUser());
        });
        return ResponseEntity.ok("przekazano poprawnie");
    }

    @RequestMapping(path = "/take", method = RequestMethod.POST)
    public ResponseEntity<?> takePrivilege(@RequestBody PrivilegeTransferRequest privilegeTransferRequest) {
        if (!privilegeService.canTakePrivilege(privilegeTransferRequest)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Nie mozna odebracuprawnien");
        }

        privilegeTransferRequest.getPrivileges().forEach(privilege -> {
            PrivilegeOnResource privilegeOnResource = privilegeService.getPrivilegeOnResource(privilege, privilegeTransferRequest.getTable());
            privilegeService.takePrivilege(
                    privilegeOnResource,
                    userService.getUserByLogin(privilegeTransferRequest.getRecipient()),
                    userService.getActiveUser());
        });

        return ResponseEntity.ok("Odebrano poprawnie");
    }

    @RequestMapping(path = "/take-over", method = RequestMethod.POST)
    public ResponseEntity<?> takeOver(@RequestBody TakeOverRequest takeOverRequest) {
        if (privilegeService.hasTakeOver(userService.getActiveUser()) &&
                userService.isAdmin(userService.getActiveUser())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Nie mozna nadac przejmowania");
        }

        privilegeService.setTakeOver(userService.getUserByLogin(takeOverRequest.getLogin()), userService.getActiveUser());

        return ResponseEntity.ok("Nadano przejmowanie poprawnie");
    }


    private void takeAllPrivilegesFromRecipient(PrivilegeTransferRequest privilegeTransferRequest) {
        Map<String, List<String>> privileges = userService.getUserPrivileges(userService.getUserByLogin(privilegeTransferRequest.getRecipient()).getId());
        for(Map.Entry<String, List<String>> pair : privileges.entrySet()) {
            for(String value : pair.getValue()) {
                PrivilegeOnResource privilegeOnResource = privilegeService.getPrivilegeOnResource(value, pair.getKey());

                privilegeService.takeAllPrivileges(
                        privilegeOnResource,
                        userService.getUserByLogin(privilegeTransferRequest.getRecipient()),
                        userService.getActiveUser()
                );
            }
        }
    }

}
