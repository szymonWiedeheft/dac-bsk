package com.dac.bsk.dac.privilege;

import com.dac.bsk.dac.PrivilegeOnResource;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by SzymonW on 6/4/2017.
 */
public interface PrivilegeOnResourceRepository extends JpaRepository<PrivilegeOnResource, Long> {
}
