package com.dac.bsk.dac.privilege;

import com.dac.bsk.dac.PrivilegeOnResource;
import com.dac.bsk.dac.UserPriviledgeHistory;
import com.dac.bsk.dac.UserPrivilegeOnResource;
import com.dac.bsk.dac.user.User;
import com.dac.bsk.dac.user.UserService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by SzymonW on 6/2/2017.
 */
@Service
public class PrivilegeService {
    private EntityManager entityManager;
    private UserService userService;
    private PrivilegeOnResourceRepository privilegeOnResourceRepository;


    public PrivilegeService(EntityManager entityManager, UserService userService, PrivilegeOnResourceRepository privilegeOnResourceRepository) {
        this.entityManager = entityManager;
        this.userService = userService;
        this.privilegeOnResourceRepository = privilegeOnResourceRepository;
    }

    public PrivilegeOnResource getPrivilegeOnResource(String privilegeName, String resourceName) {
        String query = "SELECT p From Privilege p where p.name = '" + privilegeName + "'";
        Privilege privilege = (Privilege) entityManager.createQuery(query).getSingleResult();

        return privilege.getPrivilegeOnResources().stream().filter(privilegeOnResource ->
            privilegeOnResource.getResource().getTableName().equals(resourceName)
        ).findFirst().orElse(null);
    }

    @Transactional
    public void transferPrivilege(PrivilegeOnResource privilegeOnResource, User recipient, User sender) {
        createHistory(privilegeOnResource, recipient, sender, "Przekazano prawa");

        UserPrivilegeOnResource userPrivilegeOnResource = new UserPrivilegeOnResource(
                privilegeOnResource,
                sender,
                recipient
        );
        entityManager.persist(userPrivilegeOnResource);
    }

    @Transactional
    public void createHistory(PrivilegeOnResource privilegeOnResource, User recipient, User sender, String msg) {
        UserPriviledgeHistory userPriviledgeHistory = new UserPriviledgeHistory(
                new Date(System.currentTimeMillis()),
                sender,
                recipient,
                privilegeOnResource,
                msg
        );
        entityManager.persist(userPriviledgeHistory);
    }

    public boolean isTransferAcceptable(PrivilegeTransferRequest privilegeTransferRequest) {
        return isValidUser(privilegeTransferRequest.getRecipient()) &&
                hasRightsToTransferPrivilege(userService.getActiveUser(), privilegeTransferRequest.getPrivileges(), privilegeTransferRequest.getTable()) &&
                canReceivePrivilege(userService.getUserByLogin(privilegeTransferRequest.getRecipient()), privilegeTransferRequest.getPrivileges(), privilegeTransferRequest.getTable()) &&
                !isRecipientAncestor(userService.getUserByLogin(privilegeTransferRequest.getRecipient()), userService.getActiveUser(), privilegeTransferRequest.getTable());
    }

    public boolean isValidUser(String recipient) {
        User rec = userService.getUserByLogin(recipient);
        User sender = userService.getActiveUser();

        return rec != null && rec != sender;
    }

    private boolean hasRightsToTransferPrivilege(User user, List<String> privileges, String tableName) {
        Map<String, List<String>> userPriv = userService.getUserPrivileges(user.getId());
        return !privileges.isEmpty() && privileges.stream().allMatch(privilege ->
            userPriv.getOrDefault(tableName, Collections.emptyList()).contains(privilege.toUpperCase()));
    }

    public boolean isRecipientAncestor(User recipient, User sender, String tableName) {
        String query = "select upor from UserPrivilegeOnResource upor where upor.lft = " + recipient.getId();
        List<UserPrivilegeOnResource> userPrivilegeOnResourceList = ((List<UserPrivilegeOnResource>) entityManager.createQuery(query).getResultList()).stream()
                .filter(upor -> upor.getPrivilegeOnResource().getResource().getTableName().equals(tableName))
                .collect(Collectors.toList());
        if(userPrivilegeOnResourceList.isEmpty()) { //nie ma takich, gdzie dał uprawnienia
            return false;
        }
        boolean ancestor = false;
        for(UserPrivilegeOnResource upor : userPrivilegeOnResourceList) {
            if(upor.getRgt().getId() == sender.getId()) {
                return true;
            } else {
                ancestor = ancestor | isRecipientAncestor(upor.getRgt(), sender, tableName);
            }
        }
        return ancestor;
    }

    public boolean isRecipientAncestor(User recipient, User sender) {
        String query = "select upor from UserPrivilegeOnResource upor where upor.lft = " + recipient.getId();
        List<UserPrivilegeOnResource> userPrivilegeOnResourceList = ((List<UserPrivilegeOnResource>) entityManager.createQuery(query).getResultList());
        if(userPrivilegeOnResourceList.isEmpty()) { //nie ma takich, gdzie dał uprawnienia
            return false;
        }
        boolean ancestor = false;
        for(UserPrivilegeOnResource upor : userPrivilegeOnResourceList) {
            if(upor.getRgt().getId() == sender.getId()) {
                return true;
            } else {
                ancestor = ancestor | isRecipientAncestor(upor.getRgt(), sender);
            }
        }
        return ancestor;
    }

    private boolean canReceivePrivilege(User user, List<String> privileges, String tableName) {
        Map<String, List<String>> userPriv = userService.getUserPrivileges(user.getId());
        boolean newPrivilege = !privileges.isEmpty() && privileges.stream().noneMatch(privilege ->
                userPriv.getOrDefault(tableName, Collections.emptyList()).contains(privilege.toLowerCase()) ||
                        userPriv.getOrDefault(tableName, Collections.emptyList()).contains(privilege.toUpperCase()));
        boolean updatePrivilege = !privileges.isEmpty() && privileges.stream().noneMatch(privilege ->
                !((userService.isAdmin(userService.getActiveUser()) || isChild(user, userService.getActiveUser(), tableName)) &&
                        (isPrivilegeWithGrant(getPrivilegeOnResource(privilege, tableName)) && userPriv.getOrDefault(tableName, Collections.emptyList()).contains(privilege.toLowerCase()) &&
                        !userPriv.getOrDefault(tableName, Collections.emptyList()).contains(privilege.toUpperCase()))));
        return newPrivilege || updatePrivilege;
    }

    public boolean canTakePrivilege(PrivilegeTransferRequest privilegeTransferRequest) {
        return (isValidUser(privilegeTransferRequest.getRecipient()) &&
                isChild(userService.getUserByLogin(privilegeTransferRequest.getRecipient()), userService.getActiveUser(), privilegeTransferRequest.getTable()) &&
                hasRightsFrom(userService.getUserByLogin(privilegeTransferRequest.getRecipient()), userService.getActiveUser(), privilegeTransferRequest.getTable(), privilegeTransferRequest.getPrivileges())) ||
                userService.isAdmin(userService.getActiveUser()) &&
                hasRights(userService.getUserByLogin(privilegeTransferRequest.getRecipient()), privilegeTransferRequest.getTable(), privilegeTransferRequest.getPrivileges());
    }

    public boolean isUpgradeRequest(User user, List<String> privileges, String tableName) {
        Map<String, List<String>> userPriv = userService.getUserPrivileges(user.getId());
        return !privileges.isEmpty() && privileges.stream().noneMatch(privilege ->
                !((userService.isAdmin(userService.getActiveUser()) || isChild(user, userService.getActiveUser(), tableName)) &&
                        (isPrivilegeWithGrant(getPrivilegeOnResource(privilege, tableName)) && userPriv.getOrDefault(tableName, Collections.emptyList()).contains(privilege.toLowerCase()) &&
                                !userPriv.getOrDefault(tableName, Collections.emptyList()).contains(privilege.toUpperCase()))));
    }

    private boolean hasRights(User user, String table, List<String> privileges) {
        String query = "select upor from UserPrivilegeOnResource upor where upor.rgt = " + user.getId();
        return ((List<UserPrivilegeOnResource>) entityManager.createQuery(query).getResultList()).stream()
                .filter(upor -> upor.getPrivilegeOnResource().getResource().getTableName().equals(table) &&
                        privileges.contains(upor.getPrivilegeOnResource().getPrivilege().getName()))
                .collect(Collectors.toList()).size() == privileges.size();
    }

    private boolean hasRightsFrom(User recipient, User userFrom, String tableName, List<String> privileges) {
        String query = "select upor from UserPrivilegeOnResource upor where upor.lft = " + userFrom.getId() + " and upor.rgt = " + recipient.getId();
        return ((List<UserPrivilegeOnResource>) entityManager.createQuery(query).getResultList()).stream()
                .filter(upor -> upor.getPrivilegeOnResource().getResource().getTableName().equals(tableName) &&
                    privileges.contains(upor.getPrivilegeOnResource().getPrivilege().getName()))
                .collect(Collectors.toList()).size() == privileges.size();
    }

    public boolean isChild(User child, User parent, String tableName) {
        String query = "select upor from UserPrivilegeOnResource upor where upor.lft = " + parent.getId() + " and upor.rgt = " + child.getId();
        return !((List<UserPrivilegeOnResource>) entityManager.createQuery(query).getResultList()).stream()
                .filter(upor -> upor.getPrivilegeOnResource().getResource().getTableName().equals(tableName))
                .collect(Collectors.toList()).isEmpty();
    }

    @Transactional
    public void takePrivilege(PrivilegeOnResource privilegeOnResource, User from, User sender) {
        String query = "select upor from UserPrivilegeOnResource upor where upor.rgt = " + from.getId() + " and upor.privilegeOnResource = " + privilegeOnResource.getId();
        UserPrivilegeOnResource userPrivilegeOnResource = (UserPrivilegeOnResource) entityManager.createQuery(query).getSingleResult();
        removePrivilege(userPrivilegeOnResource, sender);
        boolean hasWithGrant = (!isPrivilegeWithGrant(privilegeOnResource) && userService.hasPrivilege(
                privilegeOnResourceRepository.getOne(privilegeOnResource.getId() + 1),
                from)) || isPrivilegeWithGrant(privilegeOnResource);
        if(!isPrivilegeWithGrant(privilegeOnResource) && hasWithGrant) { //jezeli male i ma duze
            query = "select upor from UserPrivilegeOnResource upor where upor.rgt = " + from.getId() + " and upor.privilegeOnResource = " + (privilegeOnResource.getId() + 1);
            removePrivilege((UserPrivilegeOnResource) entityManager.createQuery(query).getSingleResult(), sender);
        }


        if(hasWithGrant) { //jezeli ma duze
            long privId = !isPrivilegeWithGrant(privilegeOnResource) ? privilegeOnResource.getId() + 1 : privilegeOnResource.getId() - 1;
            deletePrivFromChildren(privilegeOnResource, userPrivilegeOnResource.getRgt());
            deletePrivFromChildren(privilegeOnResourceRepository.getOne(privId), userPrivilegeOnResource.getRgt());
        }
    }

    private void removePrivilege(UserPrivilegeOnResource userPrivilegeOnResource, User requestSender) {
        UserPriviledgeHistory userPriviledgeHistory = new UserPriviledgeHistory(
                new Date(System.currentTimeMillis()),
                requestSender,
                userPrivilegeOnResource.getRgt(),
                userPrivilegeOnResource.getPrivilegeOnResource(),
                "Odebrano uprawnienia"
        );
        entityManager.persist(userPriviledgeHistory);
        entityManager.remove(userPrivilegeOnResource);
    }

    @Transactional
    public void takeAllPrivileges(PrivilegeOnResource privilegeOnResource, User from, User reciver) {
        String query = "select upor from UserPrivilegeOnResource upor where upor.rgt = " + from.getId() + " and upor.privilegeOnResource = " + privilegeOnResource.getId();
        UserPrivilegeOnResource userPrivilegeOnResource = null;

        userPrivilegeOnResource = (UserPrivilegeOnResource) entityManager.createQuery(query).getSingleResult();

        UserPriviledgeHistory userPriviledgeHistory = new UserPriviledgeHistory(
                new Date(System.currentTimeMillis()),
                reciver,
                from,
                privilegeOnResource,
                "Odebrano uprawnienia z przejmij"
        );
        entityManager.persist(userPriviledgeHistory);

        entityManager.remove(userPrivilegeOnResource);
        if(isPrivilegeWithGrant(privilegeOnResource)) {
            deletePrivFromChildren(privilegeOnResource, userPrivilegeOnResource.getRgt(), reciver);
            deletePrivFromChildren(privilegeOnResourceRepository.getOne(privilegeOnResource.getId() - 1), userPrivilegeOnResource.getRgt(), reciver);
        }
    }

    private void deletePrivFromChildren(PrivilegeOnResource privilegeOnResource, User parent) {
        String query = "select upor from UserPrivilegeOnResource upor where upor.lft = " + parent.getId() + " and upor.privilegeOnResource = " + privilegeOnResource.getId();
        List<UserPrivilegeOnResource> userPrivilegeOnResource = (List<UserPrivilegeOnResource>) entityManager.createQuery(query).getResultList();
        userPrivilegeOnResource.forEach(upor -> {
            UserPriviledgeHistory userPriviledgeHistory = new UserPriviledgeHistory(
                    new Date(System.currentTimeMillis()),
                    upor.getLft(),
                    upor.getRgt(),
                    privilegeOnResource,
                    "Odebrano uprawnienia"
            );
            entityManager.persist(userPriviledgeHistory);
            entityManager.remove(upor);

            if(isPrivilegeWithGrant(privilegeOnResource)) {
                deletePrivFromChildren(privilegeOnResource, upor.getRgt());
                deletePrivFromChildren(privilegeOnResourceRepository.getOne(privilegeOnResource.getId() - 1), upor.getRgt());
            }
        });
    }

    private void deletePrivFromChildren(PrivilegeOnResource privilegeOnResource, User parent, User reciver) {
        String query = "select upor from UserPrivilegeOnResource upor where upor.lft = " + parent.getId() + " and upor.privilegeOnResource = " + privilegeOnResource.getId();
        List<UserPrivilegeOnResource> userPrivilegeOnResource = (List<UserPrivilegeOnResource>) entityManager.createQuery(query).getResultList();
        userPrivilegeOnResource.forEach(upor -> {
            if(!(upor.getRgt().getId() == reciver.getId())) {
                UserPriviledgeHistory userPriviledgeHistory = new UserPriviledgeHistory(
                        new Date(System.currentTimeMillis()),
                        upor.getLft(),
                        upor.getRgt(),
                        privilegeOnResource,
                        "Odebrano uprawnienia"
                );
                entityManager.persist(userPriviledgeHistory);
                entityManager.remove(upor);

                if (isPrivilegeWithGrant(privilegeOnResource)) {
                    deletePrivFromChildren(privilegeOnResource, upor.getRgt());
                    deletePrivFromChildren(privilegeOnResourceRepository.getOne(privilegeOnResource.getId() - 1), upor.getRgt());
                }
            }
        });
    }

    public boolean isPrivilegeWithGrant(PrivilegeOnResource privilegeOnResource) {
        return privilegeOnResource.getId() % 2 == 0;
    }

    @Transactional
    public void setTakeOver(User user, User from) {
        PrivilegeOnResource privilegeOnResource = privilegeOnResourceRepository.getOne(41L);
        UserPriviledgeHistory userPriviledgeHistory = new UserPriviledgeHistory(
                new Date(System.currentTimeMillis()),
                from,
                user,
                privilegeOnResource,
                "przekazanie przejmij"
        );
        entityManager.persist(userPriviledgeHistory);

        UserPrivilegeOnResource userPrivilegeOnResource = new UserPrivilegeOnResource(privilegeOnResource, from, user);
        entityManager.persist(userPrivilegeOnResource);
    }

    public boolean hasTakeOver(User user) {
        String query = "select upor from UserPrivilegeOnResource upor where upor.rgt = " + user.getId() + " and upor.privilegeOnResource = 41";
        try {
            UserPrivilegeOnResource userPrivilegeOnResource = (UserPrivilegeOnResource) entityManager.createQuery(query).getSingleResult();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Transactional
    public void removeTakeOver(User userByLogin, User activeUser) {
        String query = "select upor from UserPrivilegeOnResource upor where upor.rgt = " + userByLogin.getId() + " and upor.privilegeOnResource = 41";

        UserPrivilegeOnResource userPrivilegeOnResource = (UserPrivilegeOnResource) entityManager.createQuery(query).getSingleResult();
        entityManager.remove(userPrivilegeOnResource);
    }
}
