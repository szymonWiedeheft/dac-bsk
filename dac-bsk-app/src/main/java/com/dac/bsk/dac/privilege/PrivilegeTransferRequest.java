package com.dac.bsk.dac.privilege;

import java.util.List;

/**
 * Created by SzymonW on 6/2/2017.
 */
public class PrivilegeTransferRequest {
    private String recipient;
    private String table;
    private List<String> privileges;


    public PrivilegeTransferRequest() {}

    public PrivilegeTransferRequest(String recipient, String table, List<String> privileges) {
        this.recipient = recipient;
        this.table = table;
        this.privileges = privileges;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getTable() {
        return table;
    }

    public List<String> getPrivileges() {
        return privileges;
    }
}
