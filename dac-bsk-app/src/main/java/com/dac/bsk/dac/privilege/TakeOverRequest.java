package com.dac.bsk.dac.privilege;

/**
 * Created by SzymonW on 6/5/2017.
 */
public class TakeOverRequest {
    private String login;

    public TakeOverRequest() {}

    public TakeOverRequest(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }
}
