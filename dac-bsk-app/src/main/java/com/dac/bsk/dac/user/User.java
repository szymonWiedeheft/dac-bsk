package com.dac.bsk.dac.user;


import com.dac.bsk.dac.UserPriviledgeHistory;
import com.dac.bsk.dac.UserPrivilegeOnResource;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by SzymonW on 4/7/2017.
 */
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "findByLogin",
                query = "select u from User u WHERE u.login = :login",
                resultClass = User.class
        )
})
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    @JsonIgnore
    @OneToMany(mappedBy = "lft")
    private List<UserPrivilegeOnResource> userPrivilegeOnResourcesLft;

    @JsonIgnore
    @OneToMany(mappedBy = "rgt")
    private List<UserPrivilegeOnResource> userPrivilegeOnResourcesRgt;

    @JsonIgnore
    @OneToMany(mappedBy = "userFrom")
    private List<UserPriviledgeHistory> userPriviledgeHistoryFrom;

    @JsonIgnore
    @OneToMany(mappedBy = "userTo")
    private List<UserPriviledgeHistory> userPriviledgeHistoryTo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public List<UserPrivilegeOnResource> getUserPrivilegeOnResourcesLft() {
        return userPrivilegeOnResourcesLft;
    }

    public void setUserPrivilegeOnResourcesLft(List<UserPrivilegeOnResource> userPrivilegeOnResourcesLft) {
        this.userPrivilegeOnResourcesLft = userPrivilegeOnResourcesLft;
    }

    public List<UserPrivilegeOnResource> getUserPrivilegeOnResourcesRgt() {
        return userPrivilegeOnResourcesRgt;
    }

    public void setUserPrivilegeOnResourcesRgt(List<UserPrivilegeOnResource> userPrivilegeOnResourcesRgt) {
        this.userPrivilegeOnResourcesRgt = userPrivilegeOnResourcesRgt;
    }

    public List<UserPriviledgeHistory> getUserPriviledgeHistoryFrom() {
        return userPriviledgeHistoryFrom;
    }

    public void setUserPriviledgeHistoryFrom(List<UserPriviledgeHistory> userPriviledgeHistoryFrom) {
        this.userPriviledgeHistoryFrom = userPriviledgeHistoryFrom;
    }

    public List<UserPriviledgeHistory> getUserPriviledgeHistoryTo() {
        return userPriviledgeHistoryTo;
    }

    public void setUserPriviledgeHistoryTo(List<UserPriviledgeHistory> userPriviledgeHistoryTo) {
        this.userPriviledgeHistoryTo = userPriviledgeHistoryTo;
    }
}
