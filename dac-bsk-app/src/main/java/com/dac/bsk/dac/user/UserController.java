package com.dac.bsk.dac.user;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by SzymonW on 6/1/2017.
 */
@RestController
@RequestMapping(path = "/users")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(path = "/{id}/privileges", method = RequestMethod.GET)
    public Map<String, List<String>> getAllPrivileges(@PathVariable long id) {
        return filtrDuplicate(userService.getUserPrivileges(id));
    }

    @RequestMapping(path = "/privileges", method = RequestMethod.GET)
    public Map<String, Map<String, List<String>>> getUsersPrivileges() {
        Map<String, Map<String, List<String>>> resultList = new HashMap<>();
        userService.getUsersPrivileges().forEach((k,v) -> {
            resultList.computeIfAbsent(k, value -> filtrDuplicate(v) );
        });
        return resultList;
    }

    private Map<String, List<String>> filtrDuplicate(Map<String, List<String>> privilegesMap) {
        Map<String, List<String>> resultList = new HashMap<>();
        privilegesMap.forEach((k, v) -> {
            List<String> privList = Arrays.asList("s", "i", "u", "d");
            for (String priv : privList) {
                if (v.contains(priv) && v.contains(priv.toUpperCase())) {
                    v.remove(priv);
                }
                resultList.computeIfAbsent(k, value -> v);
            }
        });
        return resultList;
    }
}
