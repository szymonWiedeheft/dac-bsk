package com.dac.bsk.dac.user;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by SzymonW on 4/7/2017.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByLogin(String login);
}
