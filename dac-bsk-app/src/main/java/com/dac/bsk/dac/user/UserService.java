package com.dac.bsk.dac.user;

import com.dac.bsk.dac.PrivilegeOnResource;
import com.dac.bsk.dac.UserPrivilegeOnResource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.*;

/**
 * Created by SzymonW on 6/1/2017.
 */
@Service
public class UserService {
    private EntityManager entityManager;
    private UserRepository userRepository;
    private UserDetailsService userDetailsService;

    public UserService(EntityManager entityManager, UserRepository userRepository, UserDetailsService userDetailsService) {
        this.entityManager = entityManager;
        this.userRepository = userRepository;
        this.userDetailsService = userDetailsService;
    }

    @SuppressWarnings("unchecked")
    public Map<String, List<String>> getUserPrivileges(long userId) {
        String query = "select pr from PrivilegeOnResource pr where pr.id in ( select upr.privilegeOnResource from UserPrivilegeOnResource upr where upr.rgt = " + userId + ")";
        Map<String, List<String>> privilegesOnResource = new LinkedHashMap<>();
        ((List<PrivilegeOnResource>) entityManager.createQuery(query).getResultList()).forEach(privilegeOnResource -> {
            privilegesOnResource.computeIfAbsent(privilegeOnResource.getResource().getTableName(), list -> new LinkedList<>())
                    .add(privilegeOnResource.getPrivilege().getName());
        });
        return privilegesOnResource;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Map<String, List<String>>> getUsersPrivileges() {
        String query = "select u from User u";
        Map<String, Map<String, List<String>>> privileges = new LinkedHashMap<>();
        ((List<User>)entityManager.createQuery(query).getResultList()).forEach(user -> {
            privileges.computeIfAbsent(user.getLogin(), map -> getUserPrivileges(user.getId()));
        });
        return privileges;
    }

    public boolean hasPrivilege(PrivilegeOnResource privilegeOnResource, User user) {
        Map<String, List<String>> userPriv = getUserPrivileges(user.getId());
        return userPriv.getOrDefault(
                privilegeOnResource.getResource().getTableName(), Collections.emptyList())
                .stream()
                .anyMatch(privilege -> privilege.equals(privilegeOnResource.getPrivilege().getName()));
    }

    public User getUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public User getActiveUser() {
        return getUserByLogin(getUserDetails().getUsername());
    }

    private UserDetails getUserDetails() {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        return userDetailsService.loadUserByUsername(name);
    }

    public boolean isAdmin(User user) {
        String query = "select upor from UserPrivilegeOnResource upor where upor.privilegeOnResource = 42";
        UserPrivilegeOnResource userPrivilegeOnResource = (UserPrivilegeOnResource) entityManager.createQuery(query).getSingleResult();
        return userPrivilegeOnResource.getRgt().getId() == user.getId();
    }
}
