package com.dac.bsk.instructor;

import com.dac.bsk.room.Room;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by SzymonW on 6/10/2017.
 */
@RestController
@RequestMapping(path = "/instructor")
public class InstructorController {
    private InstructorRepository instructorRepository;


    public InstructorController(InstructorRepository instructorRepository) {
        this.instructorRepository = instructorRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@accessController.hasPrivilege('instructor', 's') or @accessController.hasPrivilege('instructor', 'S')")
    public List<Instructor> findAll() {
        return instructorRepository.findAll().stream()
                .sorted(Comparator.comparing(Instructor::getId))
                .collect(Collectors.toList());
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("@accessController.hasPrivilege('instructor', 's') or @accessController.hasPrivilege('instructor', 'S')")
    public Instructor get(@PathVariable long id) {

        return instructorRepository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("@accessController.hasPrivilege('instructor', 'i') or @accessController.hasPrivilege('instructor', 'I') or @accessController.hasPrivilege('instructor', 'u') or @accessController.hasPrivilege('instructor', 'U')")
    public Instructor create(@RequestBody Instructor room) {
        return instructorRepository.save(room);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("@accessController.hasPrivilege('instructor', 'u') or @accessController.hasPrivilege('instructor', 'U')")
    public Instructor update(@RequestBody Instructor instructor) {
        return instructorRepository.save(instructor);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("@accessController.hasPrivilege('instructor', 'd') or @accessController.hasPrivilege('instructor', 'D')")
    public void delete(@PathVariable long id) {
        instructorRepository.delete(id);
    }
}
