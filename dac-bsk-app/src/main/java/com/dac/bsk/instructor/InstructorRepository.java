package com.dac.bsk.instructor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by SzymonW on 4/8/2017.
 */

public interface InstructorRepository extends JpaRepository<Instructor, Long> {
}
