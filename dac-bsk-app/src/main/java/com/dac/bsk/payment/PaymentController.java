package com.dac.bsk.payment;

import com.dac.bsk.course.CourseRepository;
import com.dac.bsk.customer.CustomerRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by SzymonW on 6/1/2017.
 */
@RestController
@RequestMapping(path = "/payment")
public class PaymentController {
    private PaymentRepository paymentRepository;
    private CustomerRepository customerRepository;
    private CourseRepository courseRepository;


    public PaymentController(PaymentRepository paymentRepository, CustomerRepository customerRepository, CourseRepository courseRepository) {
        this.paymentRepository = paymentRepository;
        this.customerRepository = customerRepository;
        this.courseRepository = courseRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@accessController.hasPrivilege('payment', 's') or @accessController.hasPrivilege('payment', 'S')")
    public List<PaymentDTO> findAll() {
        return paymentRepository.findAll().stream()
                .sorted(Comparator.comparing(Payment::getId))
                .collect(Collectors.toList()).stream().map(PaymentController::dbImplToDTO).collect(Collectors.toList());
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("@accessController.hasPrivilege('payment', 's') or @accessController.hasPrivilege('payment', 'S')")
    public PaymentDTO get(@PathVariable long id) {
        return dbImplToDTO(paymentRepository.findOne(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("@accessController.hasPrivilege('payment', 'i') or @accessController.hasPrivilege('payment', 'I') or @accessController.hasPrivilege('payment', 'u') or @accessController.hasPrivilege('payment', 'U')")
    public PaymentDTO create(@RequestBody PaymentDTO payment) {
        return dbImplToDTO(paymentRepository.save(dtoToDbImpl(payment)));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("@accessController.hasPrivilege('payment', 'u') or @accessController.hasPrivilege('payment', 'U')")
    public PaymentDTO update(@RequestBody PaymentDTO payment) {
        return dbImplToDTO(paymentRepository.save(dtoToDbImpl(payment)));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("@accessController.hasPrivilege('payment', 'd') or @accessController.hasPrivilege('payment', 'D')")
    public void delete(@PathVariable long id) {
        paymentRepository.delete(id);
    }

    public static PaymentDTO dbImplToDTO(Payment payment) {
        return new PaymentDTO(payment.getId(), payment.getCourse().getId(), payment.getCustomer().getId(), payment.getAmount(), payment.getType(), payment.getDate());
    }

    private  Payment dtoToDbImpl(PaymentDTO paymentDTO) {
        return new Payment(
                paymentDTO.getId(),
                courseRepository.findOne(paymentDTO.getCourse()),
                customerRepository.findOne(paymentDTO.getCustomer()),
                paymentDTO.getAmount(),
                paymentDTO.getType(),
                paymentDTO.getDate());
    }
}
