package com.dac.bsk.payment;

import com.dac.bsk.course.Course;
import com.dac.bsk.customer.Customer;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by SzymonW on 6/4/2017.
 */
public class PaymentDTO {

    private long id;

    private long course;

    private long customer;

    private double amount;

    private String type;

    private Date date;

    public PaymentDTO() {}

    public PaymentDTO(long id, long course, long customer, double amount, String type, Date date) {
        this.id = id;
        this.course = course;
        this.customer = customer;
        this.amount = amount;
        this.type = type;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCourse() {
        return course;
    }

    public void setCourse(long course) {
        this.course = course;
    }

    public long getCustomer() {
        return customer;
    }

    public void setCustomer(int customer) {
        this.customer = customer;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
