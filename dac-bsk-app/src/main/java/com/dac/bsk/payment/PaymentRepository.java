package com.dac.bsk.payment;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by SzymonW on 4/8/2017.
 */

public interface PaymentRepository extends JpaRepository<Payment, Long> {
}
