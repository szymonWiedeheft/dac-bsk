package com.dac.bsk.room;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by SzymonW on 5/31/2017.
 */
@RestController
@RequestMapping(path = "/room")
public class RoomController {

    private RoomRepository roomRepository;


    public RoomController(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@accessController.hasPrivilege('room', 's') or @accessController.hasPrivilege('room', 'S')")
    public List<Room> findAll() {
        return roomRepository.findAll().stream()
                .sorted(Comparator.comparing(Room::getId))
                .collect(Collectors.toList());
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("@accessController.hasPrivilege('room', 's') or @accessController.hasPrivilege('room', 'S')")
    public Room get(@PathVariable long id) {

        return roomRepository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("@accessController.hasPrivilege('room', 'i') or @accessController.hasPrivilege('room', 'I') or @accessController.hasPrivilege('room', 'u') or @accessController.hasPrivilege('room', 'U')")
    public Room create(@RequestBody Room room) {
        return roomRepository.save(room);
    }



    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("@accessController.hasPrivilege('room', 'u') or @accessController.hasPrivilege('room', 'U')")
    public Room update(@RequestBody Room room) {
        return roomRepository.save(room);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("@accessController.hasPrivilege('room', 'd') or @accessController.hasPrivilege('room', 'D')")
    public void delete(@PathVariable long id) {
        roomRepository.delete(id);
    }
}
