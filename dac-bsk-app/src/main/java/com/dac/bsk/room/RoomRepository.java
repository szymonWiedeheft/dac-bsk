package com.dac.bsk.room;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by SzymonW on 4/8/2017.
 */

public interface RoomRepository extends JpaRepository<Room, Long> {
}
