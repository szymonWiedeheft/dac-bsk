package com.dac.bsk.security.jwtUser;

import com.dac.bsk.security.AuthorityName;
import com.dac.bsk.dac.user.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Created by SzymonW on 4/7/2017.
 */
@Component
public class JwtUserFactory {
    public JwtUser create(User user){
        return new JwtUser(user.getId(),
                user.getLogin(),
                user.getPassword(),
                createDefaultAuthority()
                );
    }

    private List<GrantedAuthority> createDefaultAuthority() {
        return Arrays.asList(new SimpleGrantedAuthority(AuthorityName.ROLE_USER.name()));
    }
}
