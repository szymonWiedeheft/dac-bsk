package com.dac.bsk.security.jwtUser;

import com.dac.bsk.dac.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by SzymonW on 4/7/2017.
 */
@Service
public class JwtUserServiceImpl implements UserDetailsService {
    private UserRepository userRepository;
    private JwtUserFactory jwtUserFactory;

    @Autowired
    public JwtUserServiceImpl(UserRepository userRepository, JwtUserFactory jwtUserFactory) {
        this.userRepository = userRepository;
        this.jwtUserFactory = jwtUserFactory;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return jwtUserFactory.create(userRepository.findByLogin(username));
    }
}
