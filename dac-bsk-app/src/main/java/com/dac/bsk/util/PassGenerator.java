package com.dac.bsk.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by SzymonW on 4/7/2017.
 */
public class PassGenerator {

    public static void main(String args[]) {
        String pass = "user";
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hash = passwordEncoder.encode(pass);
        System.out.println(hash);
    }
}
