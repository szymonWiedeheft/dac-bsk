/*================dac=================*/
INSERT INTO users(id, name, surname, login, password, created, modified) VALUES (
  1,
  'admin',
  'admin',
  'admin',
  '$2a$10$B.wOnU3ckyHyKktLirjlmuo9EUQD6APvB6zxeKmGC2BeN3KuPWrP2',
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss'),
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss')
);

INSERT INTO privilege(id, name) VALUES (
    1,
    's'
);
INSERT INTO privilege(id, name) VALUES (
    2,
    'S'
);
INSERT INTO privilege(id, name) VALUES (
    3,
    'i'
);
INSERT INTO privilege(id, name) VALUES (
    4,
    'I'
);
INSERT INTO privilege(id, name) VALUES (
    5,
    'u'
);
INSERT INTO privilege(id, name) VALUES (
    6,
    'U'
);
INSERT INTO privilege(id, name) VALUES (
    7,
    'd'
);
INSERT INTO privilege(id, name) VALUES (
    8,
    'D'
);
INSERT INTO privilege(id, name) VALUES (
    9,
    'przejmij'
);
INSERT INTO privilege(id, name) VALUES (
    10,
    'admin'
);

INSERT INTO resource(id, table_name) VALUES (
    1,
    'room'
);
INSERT INTO resource(id, table_name) VALUES (
    2,
    'course'
);
INSERT INTO resource(id, table_name) VALUES (
    3,
    'instructor'
);
INSERT INTO resource(id, table_name) VALUES (
    4,
    'customer'
);
INSERT INTO resource(id, table_name) VALUES (
    5,
    'presence'
);
INSERT INTO resource(id, table_name) VALUES (6, 'przejmij');
INSERT INTO resource(id, table_name) VALUES (7, 'admin');


INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    1,
    1,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    2,
    2,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
   	3,
    3,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    4,
    4,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    5,
    5,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    6,
    6,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    7,
    7,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    8,
    8,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    9,
    1,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    10,
    2,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
   	11,
    3,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    12,
    4,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    13,
    5,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    14,
    6,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    15,
    7,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    16,
    8,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    17,
    1,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    18,
    2,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
   	19,
    3,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    20,
    4,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    21,
    5,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    22,
    6,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    23,
    7,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    24,
    8,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    25,
    1,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    26,
    2,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
   	27,
    3,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    28,
    4,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    29,
    5,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    30,
    6,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    31,
    7,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    32,
    8,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    33,
    1,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    34,
    2,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
   	35,
    3,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    36,
    4,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    37,
    5,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    38,
    6,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    39,
    7,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    40,
    8,
    5
);
/*przejmij*/
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    41,
    9,
    6
);
/*admin*/
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    42,
    10,
    7
);

INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (1, 1, 1, 2);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (2, 1, 1, 4);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (3, 1, 1, 6);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (4, 1, 1, 8);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (5, 1, 1, 10);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (6, 1, 1, 12);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (7, 1, 1, 14);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (8, 1, 1, 16);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (9, 1, 1, 18);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (10, 1, 1, 20);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (11, 1, 1, 22);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (12, 1, 1, 24);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (13, 1, 1, 26);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (14, 1, 1, 28);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (15, 1, 1, 30);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (16, 1, 1, 32);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (17, 1, 1, 1);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (18, 1, 1, 3);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (19, 1, 1, 5);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (20, 1, 1, 7);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (21, 1, 1, 9);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (22, 1, 1, 11);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (23, 1, 1, 13);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (24, 1, 1, 15);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (25, 1, 1, 17);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (26, 1, 1, 19);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (27, 1, 1, 21);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (28, 1, 1, 23);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (29, 1, 1, 25);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (30, 1, 1, 27);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (31, 1, 1, 29);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (32, 1, 1, 31);


INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (33, 1, 1, 42);


INSERT INTO room(id, floor, number, size) VALUES (1, '3', '301', '100.3');
INSERT INTO room(id, floor, number, size) VALUES (2, '2', '302', '30.3');
INSERT INTO room(id, floor, number, size) VALUES (3, '5', '303', '224.3');
INSERT INTO room(id, floor, number, size) VALUES (4, '7', '304', '64.3');

INSERT INTO course(id, name, start_date, end_date, category) VALUES (
	1,
    'Samba brazyliska',
    '01/05/2017',
    '31/07/2017',
    'Taniec solo'
);
INSERT INTO course(id, name, start_date, end_date, category) VALUES (
	2,
    'vzcxvz',
    '01/05/2017',
    '31/07/2017',
    'rqwer'
);
INSERT INTO course(id, name, start_date, end_date, category) VALUES (
	3,
    'rwerew',
    '01/05/2017',
    '31/07/2017',
    'kghkjg'
);


INSERT INTO customer(id, name, surname, address, phone) VALUES (
	4,
    'Katarzyna',
    'Sko',
    'Gdansk',
    '596788412'
);

INSERT INTO customer(id, name, surname, address, phone) VALUES (
	3,
    'zyna',
    'ronska',
    'Gdansk',
    '596788412'
);

INSERT INTO customer(id, name, surname, address, phone) VALUES (
	2,
    'Kayna',
    'Skoka',
    'Gdansk',
    '596788412'
);

INSERT INTO customer(id, name, surname, address, phone) VALUES (
	1,
    'Katarzyna',
    'Skowronska',
    'Gdansk',
    '596788412'
);
INSERT INTO instructor(id, name, surname, experience, employment_period) VALUES (
  1,
  'Josh',
  'Josh',
  '5 lat',
  '2 lata'
);
INSERT INTO instructor(id, name, surname, experience, employment_period) VALUES (
  2,
  'Katarzyna',
  'Katarzyna',
  '3 lat',
  '5 lat'
);
INSERT INTO instructor(id, name, surname, experience, employment_period) VALUES (
  3,
  'Kayna',
  'Josh',
  '1 lat',
  '1 lat'
);
INSERT INTO instructor(id, name, surname, experience, employment_period) VALUES (
  4,
  'Josh',
  'Josh',
  '5 lat',
  '2 lata'
);


ALTER SEQUENCE users_id_seq RESTART WITH 2;
ALTER SEQUENCE user_privilege_on_resource_id_seq RESTART WITH 34;
ALTER SEQUENCE room_id_seq RESTART WITH 5;
ALTER SEQUENCE course_id_seq RESTART WITH 5;
ALTER SEQUENCE customer_id_seq RESTART WITH 5;
ALTER SEQUENCE instructor_id_seq RESTART WITH 5;