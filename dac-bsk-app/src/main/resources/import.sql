INSERT INTO users(id, name, surname, login, password, created, modified) VALUES (
  1,
  'test',
  'test',
  'admin',
  '$2a$10$B.wOnU3ckyHyKktLirjlmuo9EUQD6APvB6zxeKmGC2BeN3KuPWrP2',
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss'),
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss')
);
