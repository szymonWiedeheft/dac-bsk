/*================dac=================*/
INSERT INTO users(id, name, surname, login, password, created, modified) VALUES (
  1,
  'admin',
  'admin',
  'admin',
  '$2a$10$B.wOnU3ckyHyKktLirjlmuo9EUQD6APvB6zxeKmGC2BeN3KuPWrP2',
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss'),
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss')
);
INSERT INTO users(id, name, surname, login, password, created, modified) VALUES (
  2,
  'josh',
  'long',
  'jlong',
  '$2a$10$/.a/qgfsPHlZ7Pcr1RxZIeoL.omuCW8Yyd4rEFmNXFspWBmmunUhi',
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss'),
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss')
);
INSERT INTO users(id, name, surname, login, password, created, modified) VALUES (
  3,
  'sz',
  'wie',
  'szwie',
  '$2a$10$/.a/qgfsPHlZ7Pcr1RxZIeoL.omuCW8Yyd4rEFmNXFspWBmmunUhi',
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss'),
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss')
);
INSERT INTO users(id, name, surname, login, password, created, modified) VALUES (
  4,
  'ola',
  'wie',
  'owie',
  '$2a$10$/.a/qgfsPHlZ7Pcr1RxZIeoL.omuCW8Yyd4rEFmNXFspWBmmunUhi',
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss'),
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss')
);

INSERT INTO privilege(id, name) VALUES (
    1,
    's'
);
INSERT INTO privilege(id, name) VALUES (
    2,
    'S'
);
INSERT INTO privilege(id, name) VALUES (
    3,
    'i'
);
INSERT INTO privilege(id, name) VALUES (
    4,
    'I'
);
INSERT INTO privilege(id, name) VALUES (
    5,
    'u'
);
INSERT INTO privilege(id, name) VALUES (
    6,
    'U'
);
INSERT INTO privilege(id, name) VALUES (
    7,
    'd'
);
INSERT INTO privilege(id, name) VALUES (
    8,
    'D'
);


INSERT INTO resource(id, table_name) VALUES (
    1,
    'room'
);
INSERT INTO resource(id, table_name) VALUES (
    2,
    'course'
);
INSERT INTO resource(id, table_name) VALUES (
    3,
    'payment'
);
INSERT INTO resource(id, table_name) VALUES (
    4,
    'customer'
);
INSERT INTO resource(id, table_name) VALUES (
    5,
    'presence'
);

INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    1,
    1,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    2,
    2,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
   	3,
    3,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    4,
    4,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    5,
    5,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    6,
    6,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    7,
    7,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    8,
    8,
    1
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    9,
    1,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    10,
    2,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
   	11,
    3,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    12,
    4,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    13,
    5,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    14,
    6,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    15,
    7,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    16,
    8,
    2
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    17,
    1,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    18,
    2,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
   	19,
    3,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    20,
    4,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    21,
    5,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    22,
    6,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    23,
    7,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    24,
    8,
    3
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    25,
    1,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    26,
    2,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
   	27,
    3,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    28,
    4,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    29,
    5,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    30,
    6,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    31,
    7,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    32,
    8,
    4
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    33,
    1,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    34,
    2,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
   	35,
    3,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    36,
    4,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    37,
    5,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    38,
    6,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    39,
    7,
    5
);
INSERT INTO privilege_on_resource(id, privilege_id, resource_id) VALUES (
    40,
    8,
    5
);


INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    1,
    1,
    2,
    1
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    2,
    1,
    2,
    7
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    3,
    1,
    2,
    6
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    4,
    1,
    2,
    15
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    5,
    1,
    2,
    20
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    6,
    1,
    2,
    33
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    7,
    1,
    2,
    28
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    8,
    1,
    3,
    2
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    9,
    1,
    3,
    4
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    10,
    1,
    3,
    32
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    11,
    1,
    3,
    28
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    12,
    1,
    4,
    8
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    13,
    3,
    4,
    2
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    14,
    1,
    4,
    28
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    15,
    3,
    4,
    32
);
INSERT INTO user_privilege_on_resource(id, lft, rgt, privilege_on_resource_id) VALUES (
    16,
    3,
    4,
    39
);
