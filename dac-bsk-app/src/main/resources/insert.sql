INSERT INTO users(id, name, surname, login, password, created, modified) VALUES (
  1,
  'test',
  'test',
  'admin',
  '$2a$10$B.wOnU3ckyHyKktLirjlmuo9EUQD6APvB6zxeKmGC2BeN3KuPWrP2',
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss'),
  to_timestamp('7/05/2017 16:30.12', 'dd/mm/yyyy hh24:mi.ss')
);

INSERT INTO room(id, floor, number, size) VALUES (1, 3, 301, 54.3);

INSERT INTO room(id, floor, number, size) VALUES (2, 2, 201, 44.3);

INSERT INTO room(id, floor, number, size) VALUES (3, 2, 201, 24.3);

INSERT INTO room(id, floor, number, size) VALUES (4, 2, 201, 14.3);

INSERT INTO room(id, floor, number, size) VALUES (5, 1, 101, 74.3);

INSERT INTO room(id, floor, number, size) VALUES (6, 5, 501, 34.3);